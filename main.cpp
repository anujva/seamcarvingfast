#include <stdio.h>
#include <fstream>
#include <math.h>
#include <stdlib.h>

using namespace std;


#define HORIZONTAL 0
#define VERTICAL 1

#define DEFAULT_BEST_COST 0x0FFFFFFF
#define USED_MASK 0x80000000
#define VALUE_MASK 0x7FFFFFFF

typedef struct { void * data; int cost; int direction;} Geodesic;
typedef struct { Geodesic *buf; int n, alloc; int lastVal; } pri_queue_t, *pri_queue;

#define priq_purge(q) (q)->n = 1
#define priq_size(q) ((q)->n - 1)
/* first element in array not used to simplify indices */
pri_queue priq_new(int size)
{
	pri_queue q = (pri_queue)malloc(sizeof(pri_queue_t));
	q->buf = (Geodesic*)malloc(sizeof(Geodesic) * size);
	q->alloc = size;
	q->n = 1;
	q->lastVal = 1;
	return q;
}

Geodesic* priq_push(pri_queue q, void *data, int cost)
{
	Geodesic *b;
	int n, m;
	
	if (q->n > q->alloc) {
		//if the value of n exceeds alloc we need to stop adding to the priority queue
        //
        b = q->buf;
        n = q->alloc;
        
        //here do we kick out the parent of the heaviest element or the element itself..?
        if(cost < b[n].cost){
            if(n%2 == 0){
                q->lastVal = n/2;
            }else{
                q->lastVal = n-1;
            }
        }
        while ((m=n/2) && cost < b[m].cost) {
            b[n] = b[m];
            n = m;
        }
        b[n].data = data;
        b[n].cost = cost;
	} else{
        b = q->buf;
        
        n = q->n++;
        /* append at end, then up heap */
        while ((m = n / 2) && cost < b[m].cost) {
            if(b[m].cost == q->buf[q->lastVal].cost){
                q->lastVal = n;
            }
            b[n] = b[m];
            n = m;
        }
        b[n].data = data;
        b[n].cost = cost;
        if(cost>q->buf[q->lastVal].cost){
            //we have to update the lastVal to whatever position this cost was entered to.. luckily its n
            q->lastVal = n;
        }
    }
    
    //this will always return the last element in the queue
    Geodesic *return_val = &q->buf[q->n-1];
    return return_val;
}

/* remove top item. returns 0 if empty. *pri can be null. */
void * priq_pop(pri_queue q, int *cost)
{
	void *out;
	if (q->n == 1) return 0;
	
	Geodesic *b = q->buf;
	
	out = b[1].data;
	if (cost) *cost = b[1].cost;
	
	/* pull last item to top, then down heap. */
	--q->n;
	
	int n = 1, m;
	while ((m = n * 2) < q->n) {
		if (m + 1 < q->n && b[m].cost > b[m + 1].cost) m++;
		
		if (b[q->n].cost <= b[m].cost) break;
		b[n] = b[m];
		n = m;
	}
	
	b[n] = b[q->n];
	if (q->n < q->alloc / 2 && q->n >= 16)
		q->buf = (Geodesic*)realloc(q->buf, (q->alloc /= 2) * sizeof(b[0]));
	
	return out;
}

/* get the top element without removing it from queue */
void* priq_top(pri_queue q, int *cost)
{
	if (q->n == 1) return 0;
	if (cost) *cost = q->buf[1].cost;
	return q->buf[1].data;
}


int main(int argc, char *argv[]){
	//lets open up an image;
	fstream myfile;
	//myfile.open("/Rahul/code/old/CppXcode/LloyedMaxScalar5b/build/Debug/elaine_decoded.raw", ios::binary|ios::in);
    myfile.open("/Rahul/code/old/CppXcode/SeamCarvingFast/shuttle.raw", ios::binary|ios::in);
	const int size = 65536;
	int image[size];
	int ii=0;
	while(myfile.good()){
		image[ii++] = myfile.get();
	}
	myfile.close();
    
	int Gx[size], Gy[size], image_energy[size];
	//image is stored.. now we use the sobel operator to get the image energy
	for(int i=1; i<255; i++){
		for(int j=1; j<255; j++){
			Gx[i*256+j] = image[(i+1)*256+j-1]+2*image[(i+1)*256+j]+image[(i+1)*256+j+1]-image[(i-1)*256+j-1]-2*image[(i-1)*256+j]-image[(i-1)*256+j+1];
			Gy[i*256+j] = image[(i-1)*256+j+1]+2*image[(i)*256+j+1]+image[(i+1)*256+j+1]-image[(i-1)*256+j-1]-2*image[(i)*256+j-1]-image[(i+1)*256+j-1];
			image_energy[i*256+j] = abs(Gx[i*256+j]) + abs(Gy[i*256+j]);
		}
	}
	
    //favor straighter lines if the costs of the neighbors are all low
    //Add a quadratic contribution to the cost
    
    for (int i=0; i<size; i++){
        int c = image_energy[i];           
        image_energy[i] = (c < 5) ? 0 :  c + ((c * c) >> 8); 
    }
    
	//image energy has been calculated..
	//now lets define the array first locations.. which will be used as the initial positions 
	//for carving out the seams
	int maxSearches = 256;
	int firstPositions[maxSearches];
	int n=0;
	
	for(int i=0;((n<maxSearches)&&(i<24)); i+=3){
		for(int j=i&7;((n<maxSearches)&&(j<256)); j+=8){
			firstPositions[n++] = j;
            //printf("Value: %d\n", firstPositions[n-1]);
		}
	}
	
	//now we have the firstPositions from where we will begin the search forthe best seams..
	//the variable stride stores the width.. I am not really sure about it right now.. but will come back to it..
	//we need to define a few variables now
	int maxAvgGeoPixCost = 256;
	int geoLength;
	int inc;
	int incLine;
	int lineLength;
	int dir = 1;
	int width= 256;
    int height = 256;
    int stride =256;
    int nbGeodesics = width*20/100;
	if(dir == HORIZONTAL){
		geoLength = width;
		lineLength = height;
		inc = stride;
		incLine = 1;
	}else{
		geoLength = height;
		lineLength = width;
		inc = 1;
		incLine = stride;
	}
	
	int maxGeo = (nbGeodesics>maxSearches)?maxSearches:nbGeodesics;
	
	//Queue geodesics based on cost
	pri_queue geodesicSortedQueue = priq_new(maxGeo);
	const int geoLength4 = geoLength & -4;
	bool consumed = true;
	Geodesic *geodesic = (Geodesic *)malloc(sizeof(Geodesic));
    geodesic->data = (void *)malloc(sizeof(int)*geoLength);
	Geodesic *last = NULL;
	int maxCost = geoLength*maxAvgGeoPixCost;
	int *costs = image_energy;
	
	for(int i=0; i<maxSearches; i++){
		if(consumed == true){
			geodesic = (Geodesic *)malloc(sizeof(Geodesic));
            geodesic->data = (void *)malloc(sizeof(int)*size);
            geodesic->cost = 0;
		}
		
		consumed = false;
		int bestLinePos = firstPositions[i];
		int costIdx = inc*bestLinePos;
        int *geoData = (int *)geodesic->data;
		geoData[0] = bestLinePos;
		geodesic->cost = costs[costIdx];
		
		//Processing each row/column
		for(int pos = 1; pos<geoLength; pos++){
			costIdx += incLine;
			int startCostIdx = costIdx;
			int bestCost = DEFAULT_BEST_COST;
			int startBestLinePos = bestLinePos;
			
			if((costs[costIdx] & USED_MASK) == 0)
				bestCost = costs[costIdx];
			
			if(bestCost > 0){
				int idx = startCostIdx -inc;
				for(int linePos = startBestLinePos-1; linePos >= 0; idx-=inc, linePos--){
					int cost = costs[idx];
					//Skip used pixels
					if((cost&USED_MASK) != 0)
						continue;
					
					if(cost<bestCost){
						bestCost = cost;
						bestLinePos = linePos;
						costIdx = idx;
					}
					
					break;
				}
			}
			
			if(bestCost>0){
				int idx = startCostIdx+inc;
				for(int linePos = startBestLinePos+1; linePos<lineLength; idx+=inc, linePos++){
					int cost = costs[idx];
					if((cost&USED_MASK)!=0)
						continue;
					
					if(cost<bestCost){
						bestCost = cost;
						bestLinePos = linePos;
						costIdx = idx;
					}
					
					break;
				}
				geodesic->cost += bestCost;
				//Skip, this path is already too expensive;
				if(geodesic->cost >= maxCost){
					break;
				}
			}
			
			geoData[pos] = bestLinePos;
		}
		
		if(geodesic->cost < maxCost){
			//Add geodesic (in increasing cost order). It is sure to succeed (it may evict the current tail)
			//because geodesic.cost<maxCost and maxCost is adjusted to tail.value.
			//to make this bit of the code work.. there needs to be some change in the standard 
			//priority queue template
            Geodesic *newLast = priq_push(geodesicSortedQueue, geoData, geodesic->cost);
            if(nbGeodesics>1){
                int startLine = 0;
                int *gp = (int *)geodesic->data;
                
                if(last!=NULL){
                    int *lp = (int *)last->data;
                    for(int k=0; k<geoLength4; k+=4){
                        costs[startLine+(inc*gp[k])] |= USED_MASK;
                        costs[startLine+(inc*lp[k])] &= VALUE_MASK;
                        startLine += incLine;
                        costs[startLine+(inc*gp[k+1])] |= USED_MASK;
                        costs[startLine+(inc*lp[k+1])] &= VALUE_MASK;
                        startLine += incLine;
                        costs[startLine+(inc*gp[k+2])] |= USED_MASK;
                        costs[startLine+(inc*lp[k+2])] &= VALUE_MASK;
                        startLine += incLine;
                        costs[startLine+(inc*gp[k+3])] |= USED_MASK;
                        costs[startLine+(inc*lp[k+3])] &= VALUE_MASK;
                        startLine += incLine;
                    }
                    for (int k=geoLength4; k<geoLength; k++)
                    {
                        costs[startLine+(inc*gp[k])] |= USED_MASK;
                        costs[startLine+(inc*lp[k])] &= VALUE_MASK;
                        startLine += incLine;
                    }
                }else{
                    for (int k=0; k<geoLength4; k+=4)
                    {
                        costs[startLine+(inc*gp[k])]   |= USED_MASK;
                        startLine += incLine;
                        costs[startLine+(inc*gp[k+1])] |= USED_MASK;
                        startLine += incLine;
                        costs[startLine+(inc*gp[k+2])] |= USED_MASK;
                        startLine += incLine;
                        costs[startLine+(inc*gp[k+3])] |= USED_MASK;
                        startLine += incLine;
                    }
                    
                    for (int k=geoLength4; k<geoLength; k++)
                    {
                        costs[startLine+(inc*gp[k])] |= USED_MASK;
                        startLine += incLine;
                    }
                }
            }
            
            if(last == NULL){
                consumed = true;
            }else{
                geodesic = last;
            }
            
            //update maxcost
            if(geodesicSortedQueue->n >= maxGeo){
                last = newLast;
                maxCost = newLast->cost;
            }
		}
        
        if(maxCost == 0 && geodesicSortedQueue->alloc == maxGeo){
            break;
        }
	}
    
    //now supposedly I should be having all the values of the geodesics in the queue.. all nice and sorted.. 
    int *c;
    int cost_c;
    int size_store = geodesicSortedQueue->n;
    printf("The value of the size of the queue is : %d\n", geodesicSortedQueue->n);
    Geodesic *geodesics_sorted_array = (Geodesic *)malloc(sizeof(Geodesic)*geodesicSortedQueue->n);
    ii= 0;
    while((c=(int*)priq_pop(geodesicSortedQueue, &cost_c))){
//        for(int i=0; i<height; i++){
//            printf("%d:The value is: %d and the value of the cost is: %d\n", i, c[i], cost_c);
//        }
        geodesics_sorted_array[ii].data = c;
        geodesics_sorted_array[ii++].cost = cost_c;
    }
    
    //now that we have the things in the queue.. we need to do the remove geodesics now..
    //will do that?
    int *test_data = (int *)geodesics_sorted_array[50].data;
    for(int i=0; i<height; i++){
        printf("%d",test_data[i]);
    }
    for(int i=0; i<size_store-1; i++){
        int *data = (int*)geodesics_sorted_array[i].data;
        for(int j=0; j<height;j++){
            printf("%d:The value is: %d and the value of the cost is: %d the value of j is: %d \n", i, data[j], geodesics_sorted_array[i].cost, j);            
            image[j*width+data[j]] = 255;
        }
    }
    
    myfile.open("/Rahul/output.raw", ios::out);
    for(int i=0; i<256*256; i++){
        myfile.put(image[i]);
    }
    myfile.close();
}

void insertionSort(int arr[], int length) {
    int i, j, tmp;
    for (i = 1; i < length; i++) {
        j = i;
        while (j > 0 && arr[j - 1] > arr[j]) {
            tmp = arr[j];
            arr[j] = arr[j - 1];
            arr[j - 1] = tmp;
            j--;
        }
    }
}

void removeGeodesics(Geodesic *geodesic, int *src, int *dst, int dir, int geodesicLength, int width, int height, int stride){
    if(geodesic == NULL){
        return;
    }
    
    int linePositions[geodesicLength];
    
    //Default values for vertical
    int endj = height;
    int endLine = width;
    int incIdx = 1;
    int incStart = stride;
    
    int color = 0xFFFF0000;
    
    if(dir == HORIZONTAL){
        endj = width;
        endLine = height;
        incIdx = stride;
        incStart = 1;
        color = 0xFF0000FF;
    }
    
    int srcStart = 0;
    int dstStart = 0;
    
    for(int j=0; j<endj; j++){
        for(int k=0; k<geodesicLength; k++){
            int *data = (int *)geodesic[k].data;
            linePositions[k] = data[j];
        }
        //I need to sort this line positions array.. 
        if(geodesicLength > 1){
            insertionSort(linePositions, geodesicLength);
        }
        
        int srcIdx = srcStart;
        int dstIdx = dstStart;
        int posIdx = 0;
        int nextPos = linePositions[posIdx];
        int endPosIdx = geodesicLength-1;
        int pos = 0;
        
        while(pos<endLine){
            int len = nextPos - pos;
            
            if(len !=0){
                if((dir == VERTICAL)&&(len>=32)){
                    
                }
            }
        }
    }
}





















